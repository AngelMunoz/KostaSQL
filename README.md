[Koa]: https://koajs.com/
[Typeorm]: https://typeorm.io/
[Winston]: https://github.com/winstonjs/winston/
[pnpm]: https://pnpm.js.org/
[pm2]: https://pm2.io/

[![pipeline status](https://gitlab.com/AngelMunoz/KostaSQL/badges/master/pipeline.svg)](https://gitlab.com/AngelMunoz/KostaSQL/commits/master)


> This project uses [pnpm] and [pm2]
> - npm i -g pnpm pm2

# Kosta With SQL
This is a small skeleton for a [Koa] application, it includes JWT Auth,
Postgres Integration via [Typeorm] and a [Winston] based Logger

Fairly simple and full async/await.

## After Cloning
- `pnpm install`

create a ecosystem.config.js file
```js
module.exports = {
  apps: [{
    name: "KostaSQL",
    script: "bin/app.js",
    watch: ['bin/**/*.js'],
    ignore_watch: ['node_modules', 'src', 'bin/tsconfig.tsbuildinfo'],
    node_args: ['--inspect'],
    env: {
      NODE_ENV: "development",
      TYPEORM_CONNECTION: "postgres",
      TYPEORM_HOST: "localhost",
      TYPEORM_USERNAME: "admin",
      TYPEORM_PASSWORD: "Admin123",
      TYPEORM_DATABASE: "kostadb",
      TYPEORM_PORT: 5432,
      TYPEORM_LOGGING: true,
      TYPEORM_MIGRATIONS_RUN: true,
      TYPEORM_SYNCHRONIZE: false,
      TYPEORM_ENTITIES: "bin/entities/*.js",
      TYPEORM_MIGRATIONS: "bin/migrations/*.js",
      TYPEORM_SUBSCRIBERS: "bin/subscribers/*.js",
      TYPEORM_ENTITIES_DIR: "src/entities",
      TYPEORM_MIGRATIONS_DIR: "src/migrations",
      TYPEORM_SUBSCRIBERS_DIR: "src/subscribers",
    }
  }]
}
```

## Development
open a terminal and run 
- `pnpm run build:watch`

on a different terminal run
- `pm2 start`

The first one will rebuild on changes, the second one will start the application in watch mode after that you can manage your app with [pm2] commands
for example to see logs 
- `pm2 logs`

If you are using vscode as your editor, you can run the commands avobe and then press <kbd>F5</kbd>


## Migrations
> for more examples please visit [migrations cli docs](https://typeorm.io/#/migrations)

To create migrations use the typeorm cli with pnpx: e.g.:
- `pnpx typeorm migrations:create -n MyNewMigration`

To run migrations use built commmand
- `pnpm run migrations:up`
- `pnpm run migrations:down`