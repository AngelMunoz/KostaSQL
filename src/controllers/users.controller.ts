import { BaseContext } from 'koa';
import { logger } from '../services/logger.service';
import { KoaNext } from '../interfaces/annotations';
import { UserService } from '../services/user.service';

export async function find(ctx: BaseContext, next: KoaNext): Promise<void> {
  await next();
  const $users = new UserService();
  const [list, count] = await $users.find();
  ctx.status = 200;
  ctx.body = { list, count };
}

export async function findOne(ctx: BaseContext, next: KoaNext): Promise<void> {
  await next();
  logger.info(`${ctx.path}`);
}

export async function update(ctx: BaseContext, next: KoaNext): Promise<void> {
  await next();
  logger.info(`${ctx.path}`);
}

export async function del(ctx: BaseContext, next: KoaNext): Promise<void> {
  await next();
  logger.info(`${ctx.path}`);
}
