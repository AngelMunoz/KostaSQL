import { BaseContext } from 'koa';
import { hash, compare } from 'bcryptjs';
import { JwtService } from '../services/jwt.service';
import { logger } from '../services/logger.service';
import { KoaNext } from '../interfaces/annotations';
import { UserService } from '../services/user.service';
import { User, IUserOptions } from '../entities/users';

export async function login(ctx: BaseContext, next: KoaNext): Promise<void> {
  await next();
  if (!ctx.request.body || !ctx.request.body.email || !ctx.request.body.password) {
    ctx.response.status = 400;
    ctx.body = { error: 'Missing Fields' };
    return;
  }
  const $users = new UserService();
  const payload: { email: string; password: string } = { ...ctx.request.body };
  let creds: User;
  try {
    creds = await $users.findForAuth(payload.email);
  } catch (error) {
    ctx.response.status = 404;
    ctx.body = { error: 'User Not Found' };
    return;
  }

  const valid = await compare(payload.password, creds.password);

  if (!valid) {
    ctx.response.status = 403;
    ctx.body = { error: 'Invalid Credentials' };
    return;
  }

  const jwts = new JwtService();
  const token = await jwts.issueToken({ user: { id: creds.id, email: creds.email } });
  ctx.body = { token, user: creds.id };
}

export async function signup(ctx: BaseContext, next: KoaNext): Promise<void> {
  await next();
  if (!ctx.request.body) {
    ctx.response.status = 400;
    ctx.body = { error: 'Missing Request Body' };
    return;
  }
  const $users = new UserService();
  const payload: IUserOptions = { ...ctx.request.body };
  payload.password = await hash(payload.password, 10);
  try {
    const user = await $users.save(payload);
    if (!user) throw new Error(`Failed to save User: [${payload.email}]`);
  } catch (error) {
    logger.warn(`[${__filename}]:[SignUp] - ${error.message}`);
    if (error.code == 23505) {
      ctx.response.status = 400;
      ctx.body = { error: `Email is already being used: [${payload.email}]` };
      return;
    }
    ctx.response.status = 422;
    ctx.body = { error: `Failed to create user: [${payload.email}]` };
    return;
  }
  ctx.response.status = 202;
}
