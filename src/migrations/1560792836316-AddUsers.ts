import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class AddUsers1560792836316 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    try {
      await queryRunner.createTable(
        new Table({
          name: 'users',
          columns: [
            { name: 'id', isPrimary: true, isGenerated: true, type: 'int' },
            { name: 'email', isUnique: true, isNullable: false, type: 'varchar', length: '120' },
            { name: 'password', isNullable: false, type: 'varchar', length: '120' },
            { name: 'name', isNullable: false, type: 'varchar', length: '120' },
            { name: 'lastName', isNullable: false, type: 'varchar', length: '120' },
            { name: 'createdAt', isNullable: true, type: 'timestamp' },
            { name: 'updatedAt', isNullable: true, type: 'timestamp' }
          ]
        }),
        true
      );
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    try {
      await queryRunner.dropTable('users', true);
    } catch (error) {
      throw new Error(error.message);
    }
  }

}
