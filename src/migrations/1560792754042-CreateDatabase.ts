import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateDatabase1560792754042 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    try {
      await queryRunner.createDatabase('kostadb', true);
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    try {
      await queryRunner.dropDatabase('kostadb', true);
    } catch (error) {
      throw new Error(error.message);
    }
  }

}
