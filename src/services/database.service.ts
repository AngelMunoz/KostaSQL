import { createConnection, Connection } from 'typeorm';

export function connectToDB(): Promise<Connection> {
  return createConnection();
}
