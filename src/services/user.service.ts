import { Repository, getRepository } from 'typeorm';
import { User, IUserOptions } from '../entities/users';
import { IPaginationParams } from '../interfaces/pagination.interface';

export class UserService {
  protected $repository: Repository<User>;
  public constructor(repository = null) {
    this.$repository = repository || getRepository(User);
  }

  public find(
    params: IPaginationParams<IUserOptions> = {
      where: {},
      page: 1,
      limit: 10
    }
  ): Promise<[User[], number]> {
    return this.$repository.findAndCount({ ...params });
  }

  public findByEmail(email: string): Promise<User> {
    return this.$repository.findOneOrFail({ where: { email } });
  }

  public findForAuth(email: string): Promise<User> {
    return this.$repository.findOneOrFail({
      where: { email },
      select: ['email', 'password']
    });
  }

  public save(user: IUserOptions): Promise<IUserOptions & User> {
    return this.$repository.save(user);
  }

}