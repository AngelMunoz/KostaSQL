import { decode, sign, verify } from 'jsonwebtoken';
import { JWT_SECRET } from '../config';

/**
 * JsonWebToken Service to issue, decode and verify tokens
 * @class
 */
export class JwtService {
  protected token: string;
  /**
   * works on top of the token if provided
   * @param {string?} token  Json Web Token provided to work with it
   */
  public constructor(token = JWT_SECRET) {
    this.token = token;
  }

  /**
   * decodes a json web token
   * @param {string} token jwt to be decoded
   */
  public decodeJwt(token = this.token): string | { [key: string]: any } {
    try {
      const payload = decode(token);
      if (payload) { return payload; }
    } catch (error) {
      throw new Error(error.message);
    }
  }

  /**
   * issues a json web token
   * @param {{ [x: string]: any }} payload Any Key/Value that you want to add to the token in question
   * @param {{ [x: string]: any }} options SignOptions from the jsonwebtoken lib
   */
  public async issueToken(payload = {}, options = { expiresIn: '1 day' }): Promise<string> {
    const token = await sign(payload, this.token, options);
    // All done.
    return token;
  }

  /**
   *
   * @param {string} token token to be verified
   * @param {{ [x: string]: any }} options VeryfyOptions from the jsonwebtoken lib
   * @return {boolean}
   */
  public verify(token = this.token, options = {}): boolean {
    let valid = false;
    try {
      valid = !!verify(token, this.token, options);
    } catch (error) {
      throw new Error(error.message);
    }
    // All done.
    return valid;
  }
}
