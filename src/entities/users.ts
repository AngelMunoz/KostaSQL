import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from "typeorm";

@Entity({ name: 'users' })
export class User {

  @PrimaryGeneratedColumn()
  public id: number;
  @Column()
  public email: string;
  @Column({ select: false })
  public password: string;
  @Column()
  public name: string;
  @Column()
  public lastName: string;
  @CreateDateColumn()
  public createdAt?: Date;
  @UpdateDateColumn()
  public updatedAt?: Date;
}

export interface IUserOptions {
  id: number;
  email: string;
  password: string;
  name: string;
  lastName: string;
  createdAt?: Date;
  updatedAt?: Date;
}