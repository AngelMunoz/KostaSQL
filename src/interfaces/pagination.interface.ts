export interface IPaginationParams<T> {
  where: Partial<T>;
  page?: number;
  limit?: number;
}