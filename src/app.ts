import 'reflect-metadata';
import Koa = require('koa');
const app = new Koa();
import helmet = require("koa-helmet");
import jwt = require('koa-jwt');
import bodyParser = require('koa-bodyparser');

import { router } from './routes';
import { JWT_SECRET, HOST, PORT } from './config';
import { connectToDB } from './services/database.service';
import { defaultLogger, logger } from './services/logger.service';

app
  .use(helmet())
  .use(bodyParser())
  .use(
    jwt({ secret: JWT_SECRET })
      .unless({ path: [/^\/auth/, /^\/public/, /^\/favicon/, /^\/check/] })
  )
  .use(router.routes())
  .use(router.allowedMethods())
  .use(defaultLogger)
  .on('error', (err, ctx): void => {
    if (ctx) {
      logger.error(`[${err}] - ${ctx.href}`);
    } else {
      logger.error(`[${err}]`);
    }
  })

async function main(): Promise<void> {
  try {
    await connectToDB();
  } catch (error) {
    process.stderr.write(`[DB Connectivity Error]: ${error.message}\n`);
    process.exit(1);
  }
  app.listen(PORT, HOST, (): void => {
    logger.info(`Server Ready at [${HOST}:${PORT}] - ${new Date().toUTCString()}`);
  });
}

process
  .on('unhandledRejection', (reason, promise): void => {
    promise.catch((err): void => {
      logger.error(`Unhandled Promise: [${reason}]: ${err.stack}`);
    });
  });

main();
